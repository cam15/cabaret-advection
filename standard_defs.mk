
CFLAGS = -O2 -g -heap-arrays -traceback -check all -fp-stack-check -mcmodel=large
NETCDF_VERSION=4.3.3
CC = ifort

LIBS = -L${MKL_HOME}/interfaces/fftw3xf -L/apps/netcdf/$(NETCDF_VERSION)/lib -L${MKL_HOME}/interfaces
LINKS = -lfftw3 -lnetcdff -lnetcdf -llapack  -lblas

HOME_DIR = /export111/work/jp1115/TEST_CODE 
NETCDF_DIR = $(HOME_DIR)/NETCDF
LAGR_DIR = $(HOME_DIR)/LAGR
VAR_DIR = $(HOME_DIR)/VAR
SRC_DIR = $(HOME_DIR)/TRANSPORT
CAB_DIR = $(HOME_DIR)/CABARET
SOLVER_DIR = $(HOME_DIR)/SOLVER
INPUT_DIR = $(HOME_DIR)/INPUT
BUILD_DIR = $(HOME_DIR)/BUILD
MISC_DIR = $(HOME_DIR)/MISC


INCLUDES = -I${MKL_HOME}/include/fftw -I${MKL_HOME}/include -I/apps/netcdf/$(NETCDF_VERSION)/include -I$(BUILD_DIR) -I$(SRC_DIR) 


